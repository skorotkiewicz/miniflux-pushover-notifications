Simple rename `config-sample.py` to `config.py` and fill with your data, then run `python notification.py`.

ProTip: You can use crontab

```
# .---------------- minute (0 - 59)
# | .------------- hour (0 - 23)
# | | .---------- day of month (1 - 31)
# | | | .------- month (1 - 12) OR jan,feb,mar,apr ...
# | | | | .----- day of week (0 - 6) (Sunday=0 or 7) OR sun,mon,tue,we$
# | | | | |
# * * * * * command to be executed
# m h dom mon dow command

*/5 * * * * /usr/bin/python /path/to/notification.py
```