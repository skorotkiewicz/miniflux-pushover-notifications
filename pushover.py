# -*- coding: utf-8 -*-

import http.client, urllib, json, sys
import config as cfg

def sendPushover(news):	
	conn = http.client.HTTPSConnection("api.pushover.net:443")
	
	conn.request("POST", "/1/messages.json",
             urllib.parse.urlencode({
				"token": cfg.pushover['token'],
				"user": cfg.pushover['user'],
				"title": "New Article!",
				"message": "{}".format(news),
				"device": cfg.pushover['device'],
                 }), { "Content-type": "application/x-www-form-urlencoded" })
	conn.getresponse()