# -*- coding: utf-8 -*-

import miniflux, json, sys
import config as cfg
import pushover as ph

client = miniflux.Client(cfg.miniflux['server'], cfg.miniflux['login'], cfg.miniflux['password'])

# Get unread feeds
feeds = client.get_entries('unread')
j = feeds

if cfg.pushover['token'] and cfg.pushover['user']:
	if j['total'] > 0:
		ph.sendPushover("You have a unread Article.")
else:
    print ("Edit your config.py first")
